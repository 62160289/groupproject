/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EmployeeManagement;
import java.util.Objects;
/**
 *
 * @author DIY
 */
public class EmployeeManagementt {
     int Id;
    String Name;
    String SurName;
    String Tel;
    String Type;
    double Sales;

    public EmployeeManagementt(int Id, String Name, String SurName, String Tel, String Type, double Sales) {
        this.Id = Id;
        this.Name = Name;
        this.SurName = SurName;
        this.Tel = Tel;
        this.Type = Type;
        this.Sales = Sales;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String SurName) {
        this.SurName = SurName;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public double getSales() {
        return Sales;
    }

    public void setSales(double Sales) {
        this.Sales = Sales;
    }

    @Override
    public String toString() {
        return "EmployeeManagementt{" + "Id=" + Id + ", Name=" + Name + ", SurName=" + SurName + ", Tel=" + Tel + ", Type=" + Type + ", Sales=" + Sales + '}';
    }
    
}

    