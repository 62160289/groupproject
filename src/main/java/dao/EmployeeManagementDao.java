/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import EmployeeManagement.EmployeeManagementt;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import poc.SelectProduct;
/**
 *
 * @author DIY
 */
public class EmployeeManagementDao implements DaoInterface<EmployeeManagementt>{

    @Override
    public int add(EmployeeManagementt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user ( name,surname,tel,type,sales )VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getType());
            stmt.setDouble(5, object.getSales());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<EmployeeManagementt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,surname,tel,type,sales FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String surname = result.getString("surname");
                String tel = result.getString("tel");
                String type = result.getString("type");
                double sales = result.getInt("sales");
                EmployeeManagementt employeeManagementt = new EmployeeManagementt(id, name, surname, tel, type,sales);
                list.add(employeeManagementt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public EmployeeManagementt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,surname,tel,type,sales FROM user WHERE id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("id");
                String name = result.getString("name");
                String surname = result.getString("surname");
                String tel = result.getString("tel");
                String type = result.getString("type");
               double sales = result.getInt("sales");
                EmployeeManagementt employeeManagementt = new EmployeeManagementt(id, name, surname, tel, type, sales);
                   return employeeManagementt;
            }
            } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    @Override
    public int delete(int id) {
    Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM user WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect now " + row);

        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(EmployeeManagementt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?,surname = ?,tel = ?,type = ?, sales = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getType());
            stmt.setDouble(5, object.getSales());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
}