/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import UserManagement.UserManagment;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Member;
import poc.SelectProduct;

/**
 *
 * @author Win 10 Home
 */
public class UserManagmentDao implements DaoInterface<UserManagment> {

    private UserManagment userManagement;

    @Override
    public int add(UserManagment object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user ( name,surname,tel,password,type )VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getPassword());
            stmt.setString(5, object.getType());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<UserManagment> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,surname,tel,password,type FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String surname = result.getString("surname");
                String tel = result.getString("tel");
                String password = result.getString("password");
                String type = result.getString("type");
                UserManagment userManagment = new UserManagment(id, name, surname, tel,password, type);
                list.add(userManagment);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public UserManagment get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,surname,tel,password,type FROM user WHERE id="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("id");
                String name = result.getString("name");
                String surname = result.getString("surname");
                String tel = result.getString("tel");
                String password = result.getString("password");
                String type = result.getString("type");
                UserManagment userManagment = new UserManagment(uid, name, surname, tel,password, type);
                return userManagment;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
    Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM user WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect now " + row);

        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(UserManagment object) {
    Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?,surname = ?,tel = ?, password = ?,type = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurName());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getPassword());
            stmt.setString(5, object.getType());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

//    public static void main(String[] args) {
//        UserManagmentDao dao = new UserManagmentDao();
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(1));
//        int id = dao.add(new UserManagment(-1,"Nut","Song","0982145678","password","E"));
//        System.out.println("id"+ id);
//        System.out.println(dao.get(id));
//        UserManagment lastUsermanagment =dao.get(id);
//        System.out.println("lastup"+lastUsermanagment);
//        lastUsermanagment.setPassword("password1");
//        int row = dao.update(lastUsermanagment);
//        UserManagment updateUserManagment = dao.get(id);
//        System.out.println("update "+updateUserManagment);
//        
//
//    }
}
