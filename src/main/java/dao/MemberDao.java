/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;



import model.Product;
import dao.DaoInterface;
import database.Database;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Member;
import poc.SelectProduct;

/**
 *
 * @author werapan
 */
 public class MemberDao implements DaoInterface<Member>{

    public MemberDao() {
       
    }

    @Override
    public int add(Member object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Member (Name,Surname,tel,point) VALUES (?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurname());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getPoint());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        } 

        db.close();
      return id;
    }

    @Override
    public ArrayList<Member> getAll() {
       ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ID, Name, Surname,tel,point FROM Member";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int ID = result.getInt("id");
                String Name = result.getString("name");
                String Surname = result.getString("Surname");
                String tel = result.getString("Tel");
                String point = result.getString("Point");
                Member member = new Member(ID,Name,Surname,tel,point );
                list.add(member);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }


    @Override
    public Member get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT ID, Name, Surname ,tel,point FROM Member WHERE ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
                int ID = result.getInt("id");
                String Name = result.getString("name");
                String Surname = result.getString("Surname");
                String tel = result.getString("Tel");
                String point = result.getString("Point");
                Member member = new Member(ID,Name,Surname,tel,point);
                return member;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Member WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect now " + row);

        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Member object) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Member SET name = ?, Surname = ?, tel = ?, point = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurname());
            stmt.setString(3, object.getTel());
            stmt.setString(4, object.getPoint());
            stmt.setInt(5, object.getID());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
     public static void main(String[] args) {
        MemberDao dao = new MemberDao();
         System.out.println(dao.getAll());
         System.out.println(dao.get(1));
         int ID =dao.add(new Member(-1,"e","e","e","e"));
         System.out.println("id:"+ID);
         System.out.println(dao.get(ID));
     }
   

    
}




   
    