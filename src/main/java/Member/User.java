/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Member;

import java.io.Serializable;

/**
 *
 * @author ROG
 */
public class User implements Serializable {
    private String ID;
    private String Name;
    private String Surname;
    private String tel;
    private String point;

    public User(String ID, String Name, String Surname, String tel, String point) {
        this.ID = ID;
        this.Name = Name;
        this.Surname = Surname;
        this.tel = tel;
        this.point = point;
    }

   

    public String getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }

    public String getTel() {
        return tel;
    }

    public String getPoint() {
        return point;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "User{" + "ID=" + ID + ", Name=" + Name + ", Surname=" + Surname + ", tel=" + tel + ", point=" + point + '}';
    }
    
}
