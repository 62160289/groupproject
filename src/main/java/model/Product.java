/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author BenZ
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private int amount;
    private String image;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Product(int pid, String name, double price, int amount) {
        this.id = pid;
        this.name = name;
        this.price = price;
        this.amount = amount;
    }
    public Product(int pid, String name, double price) {
        this(pid,name,price,0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Product(int id, String name, double price,int amount, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", amount=" + amount + ", image=" + image + '}';
    }

    public static ArrayList<Product> getProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Espresso", 40, 2,"image/1.jpg"));
        list.add(new Product(2, "Latte", 30, 2,"image/2.jpg"));
        list.add(new Product(3, "Cappuccino", 40, 2,"image/3.jpg"));
        list.add(new Product(4, "Americano", 30, 2,"image/4.jpg"));
        return list;
    }

}
