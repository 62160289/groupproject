/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ROG
 */
public class Member {
    private int ID;
    private String Name;
    private String Surname;
    private String tel;
    private String point;

    public Member(int ID, String Name, String Surname, String tel, String point) {
        this.ID = ID;
        this.Name = Name;
        this.Surname = Surname;
        this.tel = tel;
        this.point = point;
    }
    
    public Member(int ID, String Name, String tel) {
        this(ID,Name,"",tel,"");
    }
    public Member( String Name, String tel) {
        this(-1,Name,"",tel,"");
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }

    public String getTel() {
        return tel;
    }

    public String getPoint() {
        return point;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Member{" + "ID=" + ID + ", Name=" + Name + ", Surname=" + Surname + ", tel=" + tel + ", point=" + point + '}';
    }
    
}
