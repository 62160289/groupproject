/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author BenZ
 */
public class Receipt {
    private int id;
    private Date created;
    private User seller;
    private Member member;
    private ArrayList<ReceiptDetail> receiptDetail;
    
     public Receipt(int id, Date created, User seller, Member member) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.member = member;
        receiptDetail = new ArrayList<>();

    }

    public Receipt(User seller, Member member) {
        this(-1, null, seller, member);
    }

    public void addReceiptDetail(int id, Product product, int amount, double price) {
        for (int row = 0; row < receiptDetail.size(); row++) {
            ReceiptDetail r = receiptDetail.get(row);
            if (r.getProduct().getId() == product.getId()) {
                r.addAmount(amount);
                return;
            }

        }

        receiptDetail.add(new ReceiptDetail(id, product, amount, price, this));

    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1, product, amount, product.getPrice());

    }

    public void deleteReceiptDetail(int row) {
        receiptDetail.remove(row);

    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id
                + ", created=" + created
                + ", seller=" + seller
                + ", member=" + member
                + ", total=" + this.getTotal()
                + "}\n";
        for (ReceiptDetail r : receiptDetail) {
            str = str + r.toString() + "\n";
        }
        return str;
    }

    public double getTotal() {
        double total = 0;
        for (ReceiptDetail r : receiptDetail) {
            total = total + r.getTotal();

        }
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Member getMember() {
        return member;
    }

    public void setCustomer(Member member) {
        this.member = member;
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }
    
    
}
