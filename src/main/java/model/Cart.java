/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author BenZ
 */
public class Cart {
    int idProduct;
    String name;
    int amount;
    double price;
    double allPrice;

    public Cart(/*int no,*/ String name, int amount, double price, double allPrice) {
        //this.no = no;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.allPrice = allPrice;
    }

    public Cart(int idProduct, String name, int amount, double price, double allPrice) {
        this.idProduct = idProduct;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.allPrice = allPrice;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
    

    

    Cart() {
        
    }

    /*public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(double allPrice) {
        this.allPrice = allPrice;
    }

    @Override
    public String toString() {
        return "Cart{" + "name=" + name + ", amount=" + amount + ", price=" + price + ", allPrice=" + allPrice + '}';
    }

    
    
    
    
    
            
    
}
