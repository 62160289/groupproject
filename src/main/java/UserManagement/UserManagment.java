/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserManagement;

import java.util.Objects;

/**
 *
 * @author Win 10 Home
 */
public class UserManagment {
    int Id;
    String Name;
    String SurName;
    String Tel;
    String password;
    String Type;

    public UserManagment(int Id, String Name, String SurName, String Tel, String password, String Type) {
        this.Id = Id;
        this.Name = Name;
        this.SurName = SurName;
        this.Tel = Tel;
        this.password = password;
        this.Type = Type;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String SurName) {
        this.SurName = SurName;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    @Override
    public String toString() {
        return "UserManagment{" + "Id=" + Id + ", Name=" + Name + ", SurName=" + SurName + ", Tel=" + Tel + ", password=" + password + ", Type=" + Type + '}';
    }

 
    


 
    
}
